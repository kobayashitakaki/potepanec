require 'rails_helper'

RSpec.describe "potepan/products/show", type: :view do
  let(:color) { create(:option_type, name: "color") }
  let(:color_red) { create(:option_value, name: "red", option_type: color) }
  let(:color_green) { create(:option_value, name: "green", option_type: color) }
  let(:color_blue) { create(:option_value, name: "blue", option_type: color) }
  let(:size) { create(:option_type, name: "size") }
  let(:size_small) { create(:option_value, name: "small", option_type: size) }
  let(:size_medium) { create(:option_value, name: "medium", option_type: size) }
  let(:size_large) { create(:option_value, name: "large", option_type: size) }
  let(:product) { create(:product) }
  # productに関連するvariantを作成
  let!(:variants) {
    create(
      :variant, product: product,
      option_values: [
        color_red,
        color_green,
        color_blue,
        size_small,
        size_medium,
        size_large
      ]
    )
  }

  before do
    assign(:product, product)
    render
  end

  it "has h2-tag containing product name" do
    expect(rendered).to have_selector('h2' , text: /.*#{product.name}/)
  end

  it "has select-tags containing product's variants' option_type" do
    product.variant_option_values_by_option_type.each do |type, values|
      expect(rendered).to have_selector("select[name*=#{type.name}]")
    end
  end

  it "has option-tags containing product's variants' option_values" do
    product.variant_option_values_by_option_type.each do |type, values|
      values.each do |value|
        expect(rendered).to have_selector('option' , text: value.name)
      end
    end
  end

  it "displays product's price with currency" do
    # main content section
    expect(rendered).to have_selector('section.mainContent h3', text: product.display_price.to_s)

    # product quick view modal
    expect(rendered).to have_selector('#quick-view h3', text: product.display_price.to_s)
  end

end
