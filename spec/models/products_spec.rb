require 'rails_helper'

describe Spree::Product, type: :model do

  context "getting related_products" do
    # taxons
    let!(:shirts_taxon) { create(:taxon, name: "shirts") }
    let!(:ruby_taxon) { create(:taxon, name: "ruby") }
    let!(:bags_taxon) { create(:taxon, name: "bags") }

    # taxonに所属するproductの作成
    let!(:ruby_shirts_product) { create(:product, taxons: [ruby_taxon, shirts_taxon]) }
    let!(:another_ruby_shirts_product) { create(:product, taxons: [ruby_taxon, shirts_taxon]) }
    let!(:shirts_product) { create(:product, taxons: [shirts_taxon]) }
    let!(:ruby_product) { create(:product, taxons: [ruby_taxon]) }
    let!(:bags_product) { create(:product, taxons: [bags_taxon]) }

    # bags taxonにMAX_RELATED_PRODUCTS_SIZEを超える数のproductを作成
    let(:MAX_RELATED_PRODUCTS_SIZE) { 8 }
    let!(:many_bags_products) { create_list(:product, MAX_RELATED_PRODUCTS_SIZE+1, taxons: [bags_taxon]) }

    it "returns products belonging same taxon of given product" do
      related_products = Spree::Product.related_products(ruby_shirts_product)
        .sort_by_newest
        .limit_for_related_products
      expect(related_products).to include(another_ruby_shirts_product)
      expect(related_products).to include(shirts_product)
      expect(related_products).to include(ruby_product)
    end

    it "doesn't return given product itself" do
      related_products = Spree::Product.related_products(ruby_shirts_product)
        .sort_by_newest
        .limit_for_related_products
      expect(related_products).not_to include(ruby_shirts_product)
    end

    it "limits related_products size" do
      related_products = Spree::Product.related_products(bags_product)
        .sort_by_newest
        .limit_for_related_products
      expect(related_products.size).to eq(MAX_RELATED_PRODUCTS_SIZE)
    end

    it "orders related_products descendent by available_on" do
      # available_onが異なるproductの作成
      other_bags_products = []
      (1..4).to_a.shuffle!.each do |n|
        other_bags_products << create(:product, taxons: [bags_taxon], available_on: (1.hour.ago - n.minute))
      end
      # available_onの降順でソート
      other_bags_products.sort_by!{ |p| p.available_on }.reverse!

      related_products = Spree::Product.related_products(bags_product)
        .sort_by_newest
        .limit_for_related_products
      # ソート済みの配列と一致することを確認
      other_bags_products.each_with_index do |product, index|
        expect(related_products[index]).to eq(product)
      end
    end

  end

end
