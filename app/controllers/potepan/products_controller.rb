class Potepan::ProductsController < ApplicationController

  def show
    @product = Spree::Product.friendly.find(params[:id])
    @related_products = Spree::Product.related_products(@product)
      .sort_by_newest
      .limit_for_related_products
    @taxons = get_taxons(@product)
  end

  private
  def get_taxons(product)
    if params[:taxon_id]
      Spree::Taxon.find(params[:taxon_id]).self_and_ancestors
    elsif product.taxons.present?
      product.taxons.first.self_and_ancestors
    end
  end

end
