require 'rails_helper'

RSpec.describe "categories show", type: :request do
  let(:taxon) { create(:taxon) }
  let!(:products) { create_list(:product, 3, taxons: [taxon]) }
  before do
    get potepan_category_path(taxon.id)
  end

  it "responds with status code 200" do
    expect(response).to have_http_status(:ok)
  end

  it "renders show template" do
    expect(response).to render_template :show
  end

  it "has taxon name in response" do
    expect(response.body).to include(taxon.name)
  end

  it "has name of belonging products" do
    taxon.products.each do |product|
      expect(response.body).to include(product.name)
    end
  end

end
