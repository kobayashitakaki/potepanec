require 'rails_helper'

RSpec.describe "products show", type: :request do
  let(:product) { create(:product) }
  before do
    get potepan_product_path(product.friendly_id)
  end

  it "responds with status code 200" do
    expect(response).to have_http_status(:ok)
  end

  it "renders show template" do
    expect(response).to render_template :show
  end

  it "has product name in response" do
    expect(response.body).to include(product.name)
  end

  it "has product price in response" do
    expect(response.body).to include(product.display_price.to_s)
  end

  it "has product description in response" do
    expect(response.body).to include(product.description)
  end

end
