require 'rails_helper'

RSpec.describe "potepan/categories/show", type: :view do
  let(:categories) { create(:taxonomy, name: "categories") }
  let(:brands) { create(:taxonomy, name: "brands") }
  let(:taxonomies) { [categories, brands] }

  # categoriesのtaxon tree
  let(:clothing) { create(:taxon, name: "clothing", taxonomy: categories, parent: categories.root) }
  let(:bags) { create(:taxon, name: "bags", taxonomy: categories, parent: categories.root) }
  let(:shirts) { create(:taxon, name: "shirts", taxonomy: categories, parent: clothing) }
  let(:jeans) { create(:taxon, name: "jeans", taxonomy: categories, parent: clothing) }

  # brandsのtaxon tree
  let(:ruby) { create(:taxon, name: "ruby", taxonomy: brands, parent: brands.root) }

  # taxonに所属するproductsを作成
  let!(:bags_products) { create_list(:product, 3, taxons: [bags]) }
  let!(:shirts_products) { create_list(:product, 3, taxons: [shirts]) }
  let!(:jeans_products) { create_list(:product, 3, taxons: [jeans]) }
  let!(:ruby_products) { create_list(:product, 3, taxons: [ruby]) }

  before do
    assign(:taxonomies, taxonomies)
    assign(:taxon, shirts)
    assign(:products, shirts_products)
    assign(:display, 'grid')
  end

  it "has product boxes for products belong to the taxon" do
    render
    expect(rendered).to have_selector('div.productBox',
      count: shirts.products.size)
  end

  it "has sidebar panel for taxonomies" do
    render
    taxonomies.each do |taxonomy|
      expect(rendered).to have_selector("div.panel-heading", text: taxonomy.name)
    end
  end

  it "has taxon lists in sidebar panel" do
    render
    taxonomies.each do |taxonomy|
      taxonomy.root.children.each do |taxon|
        if taxon.children.blank?
          # アコーディオンは作られない
          # 所属するproducts数が出力されること
          expect(rendered).to have_selector(
            "ul.side-nav li a",
            text: /#{taxon.name}.*#{taxon.products.size}/
          )
        else
          taxon.children.each do |child|
            # アコーディオンの中身
            # 子taxonのnameとproductsの数
            expect(rendered).to have_selector(
              "ul.side-nav li ul#taxon-#{taxon.id} li",
              text: /#{child.name}.*#{child.products.size}/
            )
            # 子taxonへのリンク
            expect(rendered).to have_selector(
              "ul.side-nav li ul#taxon-#{taxon.id} li a[href*='#{child.id}']"
            )
          end
        end
      end
    end
  end

  it "has taxon name in breadcrumb" do
    render
    expect(rendered).to have_selector("ol.breadcrumb li.active", text: shirts.name)
  end

  it "has links for ancestors in breadcrumb" do
    render
    shirts.ancestors.each do |ancestor|
      expect(rendered).to have_selector("ol.breadcrumb li", text: ancestor.name)
      expect(rendered).to have_selector(
        "ol.breadcrumb li a[href*='#{ancestor.id}']"
      )
    end
  end

  context "list display" do
    before do
      assign(:display, 'list')
    end

    it "has product cards for products belong to the taxon" do
      render
      expect(rendered).to have_selector('div.productCard',
        count: shirts.products.size)
    end
  end

end
