module ApplicationHelper
  # ページのタイトルを返す
  def full_title(title = '')
    base_title = 'Potepanec'
    title.blank? ? base_title : "#{base_title} - #{title}"
  end
end
