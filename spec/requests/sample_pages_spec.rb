require 'rails_helper'

RSpec.describe "Sample pages", type: :request do

  it "renders sample/index by GET potepan_path" do
    get potepan_path
    expect(response).to have_http_status(:ok)
    expect(response).to render_template 'sample/index'
  end

  it "renders sample/index by GET potepan_index_path" do
    get potepan_index_path
    expect(response).to have_http_status(:ok)
    expect(response).to render_template 'sample/index'
  end

  it "renders sample/product_grid_left_sidebar by GET potepan_product_grid_left_sidebar_path" do
    get potepan_product_grid_left_sidebar_path
    expect(response).to have_http_status(:ok)
    expect(response).to render_template 'sample/product_grid_left_sidebar'
  end

  it "renders sample/product_list_left_sidebar by GET potepan_product_list_left_sidebar_path" do
    get potepan_product_list_left_sidebar_path
    expect(response).to have_http_status(:ok)
    expect(response).to render_template 'sample/product_list_left_sidebar'
  end

  it "renders sample/single_product by GET potepan_single_product_path" do
    get potepan_single_product_path
    expect(response).to have_http_status(:ok)
    expect(response).to render_template 'sample/single_product'
  end

  it "renders sample/cart_page by GET potepan_cart_page_path" do
    get potepan_cart_page_path
    expect(response).to have_http_status(:ok)
    expect(response).to render_template 'sample/cart_page'
  end

  it "renders sample/checkout_step_1 by GET potepan_checkout_step_1_path" do
    get potepan_checkout_step_1_path
    expect(response).to have_http_status(:ok)
    expect(response).to render_template 'sample/checkout_step_1'
  end

  it "renders sample/checkout_step_2 by GET potepan_checkout_step_2_path" do
    get potepan_checkout_step_2_path
    expect(response).to have_http_status(:ok)
    expect(response).to render_template 'sample/checkout_step_2'
  end

  it "renders sample/checkout_step_3 by GET potepan_checkout_step_3_path" do
    get potepan_checkout_step_3_path
    expect(response).to have_http_status(:ok)
    expect(response).to render_template 'sample/checkout_step_3'
  end

  it "renders sample/checkout_complete by GET potepan_checkout_complete_path" do
    get potepan_checkout_complete_path
    expect(response).to have_http_status(:ok)
    expect(response).to render_template 'sample/checkout_complete'
  end

  it "renders sample/blog_left_sidebar by GET potepan_blog_left_sidebar_path" do
    get potepan_blog_left_sidebar_path
    expect(response).to have_http_status(:ok)
    expect(response).to render_template 'sample/blog_left_sidebar'
  end

  it "renders sample/blog_right_sidebar by GET potepan_blog_right_sidebar_path" do
    get potepan_blog_right_sidebar_path
    expect(response).to have_http_status(:ok)
    expect(response).to render_template 'sample/blog_right_sidebar'
  end

  it "renders sample/blog_single_left_sidebar by GET potepan_blog_single_left_sidebar_path" do
    get potepan_blog_single_left_sidebar_path
    expect(response).to have_http_status(:ok)
    expect(response).to render_template 'sample/blog_single_left_sidebar'
  end

  it "renders sample/blog_single_right_sidebar by GET potepan_blog_single_right_sidebar_path" do
    get potepan_blog_single_right_sidebar_path
    expect(response).to have_http_status(:ok)
    expect(response).to render_template 'sample/blog_single_right_sidebar'
  end

  it "renders sample/potepan_about_us by GET potepan_potepan_about_us_path_path" do
    get potepan_about_us_path
    expect(response).to have_http_status(:ok)
    expect(response).to render_template 'sample/about_us'
  end

  it "renders sample/potepan_tokushoho by GET potepan_potepan_tokushoho_path_path" do
    get potepan_tokushoho_path
    expect(response).to have_http_status(:ok)
    expect(response).to render_template 'sample/tokushoho'
  end

  it "renders sample/privacy_policy by GET potepan_potepan_tokushoho_path_path" do
    get potepan_privacy_policy_path
    expect(response).to have_http_status(:ok)
    expect(response).to render_template 'sample/privacy_policy'
  end

end
