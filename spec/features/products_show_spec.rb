require "rails_helper"

RSpec.feature "products show" do
  # taxonomies
  let(:categories_taxonomy) { create(:taxonomy, name: "categories") }
  let(:brands_taxonomy) { create(:taxonomy, name: "brands") }

  # taxons
  let!(:clothing_taxon) { create(:taxon, name: "clothing", taxonomy: categories_taxonomy, parent: categories_taxonomy.root) }
  let!(:shirts_taxon) { create(:taxon, name: "shirts", taxonomy: categories_taxonomy, parent: clothing_taxon) }
  let!(:ruby_taxon) { create(:taxon, name: "ruby", taxonomy: brands_taxonomy, parent: brands_taxonomy.root) }

  # taxonに所属するproductの作成
  let!(:ruby_shirts_product) { create(:product, taxons: [ruby_taxon, shirts_taxon]) }
  let!(:another_ruby_shirts_product) { create(:product, taxons: [ruby_taxon, shirts_taxon]) }
  let!(:ruby_product) { create(:product, taxons: [ruby_taxon]) }
  let!(:shirts_product) { create(:product, taxons: [shirts_taxon]) }

  scenario "Product has related products" do
    visit potepan_product_path(ruby_shirts_product.friendly_id)
    within ".relatedProducts" do
      # 同じtaxonに所属するproductが表示されること
      expect(page).to have_selector(".productBox", count: 3)
      expect(page).to have_content(another_ruby_shirts_product.name)
      expect(page).to have_content(ruby_product.name)
      expect(page).to have_content(shirts_product.name)
      # ページのメインと重複したproductは表示されないこと
      expect(page).not_to have_content(ruby_shirts_product.name)
    end
  end

  scenario "Product's taxon has taxon tree" do
    # パンくずリストの表示確認
    visit potepan_product_path(shirts_product.friendly_id)
    within ".breadcrumb" do
      # product nameが出力されていること
      expect(page).to have_selector("li.active", text: shirts_product.name)
      # taxon treeのリンクが出力されていること
      taxons = shirts_product.taxons.first.self_and_ancestors
      taxons.each do |taxon|
        expect(page).to have_link taxon.name
      end
    end
  end

  scenario "User goes to another product by related_products link" do
    visit potepan_product_path(ruby_product.friendly_id)
    within ".relatedProducts" do
      click_link ruby_shirts_product.name
    end
    expect(page).to have_current_path(potepan_product_path(ruby_shirts_product.friendly_id))
  end

  scenario "User goes to category page by breadcrumb link" do
    visit potepan_product_path(ruby_product.friendly_id)
    within ".breadcrumb" do
      click_link ruby_taxon.name
    end
    expect(page).to have_current_path(potepan_category_path(ruby_taxon.id))
  end

  scenario "User goes to category page by 一覧ページへ戻る link" do
    visit potepan_product_path(ruby_product.friendly_id)
    click_link "一覧ページへ戻る"
    expect(page).to have_current_path(potepan_category_path(ruby_taxon.id))
  end

  scenario "User visits a product page from categories page" do
    # shirts taxonからruby_shirts_productページに移動した場合
    visit potepan_product_path(
      ruby_shirts_product.friendly_id, taxon_id: shirts_taxon.id
    )
    taxons = shirts_taxon.self_and_ancestors
    within ".breadcrumb" do
      # shirts taxon treeのリンクが出力されること
      taxons.each do |taxon|
        expect(page).to have_link taxon.name
      end
    end
    # shirts taxonへのリンクが出力されること
    expect(page).to have_link("一覧ページへ戻る", href: potepan_category_path(shirts_taxon.id))

    # ruby taxonからruby_shirts_productページに移動した場合
    visit potepan_product_path(
      ruby_shirts_product.friendly_id, taxon_id: ruby_taxon.id
    )
    taxons = ruby_taxon.self_and_ancestors
    within ".breadcrumb" do
      # ruby taxon treeのリンクが出力されること
      taxons.each do |taxon|
        expect(page).to have_link taxon.name
      end
    end
    # ruby taxonへのリンクが出力されること
    expect(page).to have_link("一覧ページへ戻る", href: potepan_category_path(ruby_taxon.id))
  end

end
