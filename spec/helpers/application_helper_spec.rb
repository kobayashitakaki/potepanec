require 'rails_helper'

describe ApplicationHelper do
  include ApplicationHelper

  describe "full_title" do
    it "returns base_title when blank is provided" do
      expect(full_title).to eq 'Potepanec'
    end

    it "returns conbination of provided title and base_title" do
      title = 'hogehoge'
      expect(full_title(title)).to eq "Potepanec - #{title}"
    end
  end

end
