class Potepan::CategoriesController < ApplicationController
  before_action :set_display, only: :show

  def show
    @taxon = Spree::Taxon.find(params[:id])
    @products = Spree::Product.in_taxon(@taxon)
    @taxonomies = Spree::Taxonomy.all.includes(:root)
  end

  private
  # 表示形式の設定
  def set_display
    @display ||=
      if params[:display] == 'list'
        'list'
      else
        'grid'
      end
  end

end
