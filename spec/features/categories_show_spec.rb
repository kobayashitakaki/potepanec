require 'rails_helper'

RSpec.feature "categories show" do
  let(:categories_taxonomy) { create(:taxonomy, name: "categories") }
  let(:brands_taxonomy) { create(:taxonomy, name: "brands") }

  # categoriesのtaxon tree
  let(:clothing_taxon) { create(:taxon, name: "clothing", taxonomy: categories_taxonomy, parent: categories_taxonomy.root) }
  let(:bags_taxon) { create(:taxon, name: "bags", taxonomy: categories_taxonomy, parent: categories_taxonomy.root) }
  let(:shirts_taxon) { create(:taxon, name: "shirts", taxonomy: categories_taxonomy, parent: clothing_taxon) }
  let(:jeans_taxon) { create(:taxon, name: "jeans", taxonomy: categories_taxonomy, parent: clothing_taxon) }

  # brandsのtaxon tree
  let(:ruby_taxon) { create(:taxon, name: "ruby", taxonomy: brands_taxonomy, parent: brands_taxonomy.root) }

  # taxonに所属するproductsを作成
  let!(:bags_products) { create_list(:product, 3, taxons: [bags_taxon]) }
  let!(:shirts_products) { create_list(:product, 3, taxons: [shirts_taxon]) }
  let!(:jeans_products) { create_list(:product, 3, taxons: [jeans_taxon]) }
  let!(:ruby_products) { create_list(:product, 3, taxons: [ruby_taxon]) }

  background do
    visit potepan_category_path(shirts_taxon.id)
  end

  scenario "User goes to a product page from category page" do
    # productページに移動できること
    product = shirts_taxon.products.first
    click_link product.name
    expect(page).to have_current_path(potepan_product_path(product.slug, taxon_id: shirts_taxon.id))
  end

  scenario "User goes to another taxon by breadcrumb link" do
    # 親taxonページに移動できること
    within "ol.breadcrumb" do
      click_link clothing_taxon.name
      expect(page).to have_current_path(potepan_category_path(clothing_taxon.id))
    end
  end

  xscenario "User changes display style", js: true do
    within ".filterArea" do
      find("button", text: "List").click
      # display=listが設定されること
      expect(page).to have_current_path(
        potepan_category_path(shirts_taxon.id, display: "list")
      )
      find("button", text: "Grid").click
      # display=gridが設定されること
      expect(page).to have_current_path(
        potepan_category_path(shirts_taxon.id, display: "grid")
      )
    end
  end

  xscenario "User clicks taxonomy accordion", js: true do
    within "ul.side-nav" do
      # アコーディオンの中が表示されていないこと
      expect(find("li ul")).to_not be_visible
      # depth1のtaxonをクリックするとアコーディオンの中が表示されること
      find("li", text: "#{clothing_taxon.name}").click
      expect(find("li ul")).to be_visible
      # 子taxonページに移動できること
      click_link shirts_taxon.name
      expect(page).to have_current_path(
        potepan_category_path(shirts_taxon.id)
      )
    end
  end

  context "User changed display style" do
    xscenario "User goes to another taxon by accordion", js: true do
      # 表示形式を変更
      find(".filterArea button", text: "Grid").click
      # アコーディオンメニューをクリック
      within "ul.side-nav" do
        find("li", text: "#{clothing_taxon.name}").click
        click_link shirts_taxon.name
      end
      # 移動先のページにパラメータが渡されること
      expect(page).to have_current_path(
        potepan_category_path(shirts_taxon.id, display: "grid")
      )
    end
  end

end
