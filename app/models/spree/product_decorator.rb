Spree::Product.class_eval do
  MAX_RELATED_PRODUCTS_SIZE = 8
  scope :related_products, -> (product) {
    where(
      id: Spree::Product.includes(:taxons)
        .references(:spree_products_taxons)
        .where("spree_products_taxons.taxon_id": product.taxons.ids)
        .distinct.ids
    )
    .where.not(id: product.id)
  }

  scope :sort_by_newest, -> {
    order(available_on: :desc)
  }

  scope :limit_for_related_products, -> {
    limit(MAX_RELATED_PRODUCTS_SIZE)
  }
end
